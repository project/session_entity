<?php

namespace Drupal\session_entity;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\ContentEntityNullStorage;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * SessionEntitySessionStorage class.
 */
class SessionEntitySessionStorage extends ContentEntityNullStorage {

  /**
   * Returns the tempstore.private service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Returns the current_user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a SessionEntitySessionStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Provides an interface for an entity type and its metadata.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Provides an interface for an entity field manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Defines an interface for cache implementations.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   Defines an interface for memory cache implementations.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   Provides an interface for an entity type bundle info.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   Creates a PrivateTempStore object for a given collection.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Defines an account interface which represents the current user.
   */
  public function __construct(EntityTypeInterface $entity_type,
      EntityFieldManagerInterface $entity_field_manager,
      CacheBackendInterface $cache,
      MemoryCacheInterface $memory_cache,
      EntityTypeBundleInfoInterface $entity_type_bundle_info,
      PrivateTempStoreFactory $temp_store_factory,
      AccountInterface $current_user) {
    parent::__construct($entity_type, $entity_field_manager, $cache, $memory_cache, $entity_type_bundle_info);

    $this->tempStoreFactory = $temp_store_factory;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_field.manager'),
      $container->get('cache.entity'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $container->get('tempstore.private'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function load($id) {
    // Load the entity from the user's private tempstore.
    $per_user_private_temp_store = $this->tempStoreFactory->get('session_entity');
    return $per_user_private_temp_store->get('session_entity');
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    if ($this->currentUser->isAnonymous()) {
      // Force a session to be started for anonymous users. Workaround for a
      // core bug.
      // @todo Remove this when https://www.drupal.org/node/2743931 is fixed.
      $_SESSION['session_entity_forced'] = '';
    }

    // Save the entity to the user's private tempstore.
    $per_user_private_temp_store = $this->tempStoreFactory->get('session_entity');
    $per_user_private_temp_store->set('session_entity', $entity);
  }

}
