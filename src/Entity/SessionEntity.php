<?php

namespace Drupal\session_entity\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the session entity class.
 *
 * @ContentEntityType(
 *   id = "session",
 *   label = @Translation("Session"),
 *   label_singular = @Translation("session"),
 *   label_plural = @Translation("sessions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count session",
 *     plural = "@count sessions"
 *   ),
 *   bundle_label = @Translation("Session type"),
 *   handlers = {
 *     "storage" = "Drupal\session_entity\SessionEntitySessionStorage",
 *     "form" = {
 *       "default" = "Drupal\session_entity\Form\SessionEntitySessionForm",
 *       "edit" = "Drupal\session_entity\Form\SessionEntitySessionForm"
 *     },
 *   },
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "sid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/session/{session}",
 *     "edit-form" = "/session/{session}/edit",
 *   },
 *   field_ui_base_route = "session_entity.settings"
 * )
 */
class SessionEntity extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public function delete() {
    parent::delete();

    /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $per_user_private_temp_store */
    $per_user_private_temp_store = \Drupal::service('tempstore.private');
    $per_user_private_temp_store->get('session_entity')->delete('session_entity');
  }

}
