<?php

namespace Drupal\session_entity;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service to get the current user's session entity.
 */
class SessionEntityCurrentSessionEntity {

  /**
   *  Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|object
   */
  protected $sessionEntityStorage;

  /**
   * Constructs a new instance of the SessionEntityCurrentSessionEntity.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type plugin manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->sessionEntityStorage = $entity_type_manager->getStorage('session');
  }

  /**
   * Get the session entity for the current user.
   *
   * @return \Drupal\session_entity\Entity\SessionEntity|null
   *   The session entity for the current user, or NULL if the current user does
   *   not have one.
   */
  public function getCurrentUserSessionEntity() {
    return $this->sessionEntityStorage->load(NULL);
  }

}
