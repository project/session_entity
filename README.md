# Session Entity

Session Entity provides entities which are stored in the user's session. Each
user has one entity, which they may edit (provided they have the permission).
This is a normal content entity, which may have fields added to it.

This allows anonymous users to create content which will automatically expire
when their session expires.

The module uses a custom entity storage controller to store entity data in
the private tempstore.

Example uses include allowing site visitors to select preferences or set details
about themselves such as a location.

To retrieve the session entity for the current user:

```
  $session_entity = \Drupal::service('session_entity.current')->getCurrentUserSessionEntity();
```

For a full description of the module, visit the
[project page](https://www.drupal.org/project/session_entity).

 * To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/session_entity).


## Table of contents

- Requirements
- Installation
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers

- Joachim Noreiko - [joachim](https://www.drupal.org/u/joachim)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
